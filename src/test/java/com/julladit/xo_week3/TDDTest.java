/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.xo_week3;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author acer
 */
public class TDDTest {

    public TDDTest() {
    }
//add(1,2)->3

    @Test
    public void testAdd_1_2is3() {
        assertEquals(3, Example.add(1, 2));
    }
//add(3,4)->7

    @Test
    public void testAdd_3_4is7() {
        assertEquals(7, Example.add(3, 4));
    }
//add(20,22)->42

    @Test
    public void testAdd_20_22is42() {
        assertEquals(44, Example.add(20, 22));
    }

    @Test
    public void testChop_p1_p_p2_p_is_draw() {
        assertEquals("draw", Example.chup('p', 'p'));
    }

    @Test
    public void testChop_h1_h_p2_p_is_draw() {
        assertEquals("draw", Example.chup('h', 'h'));
    }

    @Test
    public void testChop_p1_s_p2_p_is_draw() {
        assertEquals("draw", Example.chup('s', 's'));
    }

    @Test
    public void testChop_p1_s_p2_p_is_p1() {
        assertEquals("draw", Example.chup('s', 's'));
    }

    @Test
    public void testChop_p1_s_p2_p_is_p1() {
        assertEquals("p1", Example.chup('s', 'p'));
    }

    @Test
    public void testChop_p1_h_p2_s_is_p1() {
        assertEquals("draw", Example.chup('h', 's'));
    }

    @Test
    public void testChop_p1_p_p2_h_is_p1() {
        assertEquals("draw", Example.chup('p', 'h'));
    }

    @Test
    public void testChop_p1_h_p2_p_is_p2() {
        assertEquals("p2", Example.chup('h', 'p'));
    }

    @Test
    public void testChop_p1_p_p2_s_is_p2() {
        assertEquals("p2", Example.chup('p', 's'));
    }

    @Test
    public void testChop_p1_s_p2_h_is_p2() {
        assertEquals("p2", Example.chup('s', 'h'));
    }
}
